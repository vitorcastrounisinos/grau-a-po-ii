#ifndef ALUNO_H
#define ALUNO_H
#include "Pessoa.h"
#include "Curso.h"

class Curso;

class Aluno : public Pessoa
{
    public:
        Aluno();
        Aluno (std::string, std::string, std::string, std::string, Curso*,
                std::string, std::string , std::string, std::string, std::string, std::string, std::string);
        virtual ~Aluno();

        void setMatriculaAluno(std::string matriculaAluno);
        std::string getMatriculaAluno();

        void setAnoInicio(std::string anoInicio);
        std::string getAnoInicio();

        void setSemestreInicio(std::string semestreInicio);
        std::string getSemestreInicio();

        void setSituacao(std::string situacao);
        std::string getSituacao();

        void setCurso(Curso* curso);
        Curso* getCurso();

        void setAluno(std::string, std::string, std::string, std::string, Curso*,
                      std::string, std::string , std::string, std::string, std::string, std::string, std::string);

    private:
      std::string MatriculaAluno;
      std::string AnoInicio;
      std::string SemestreInicio;
      std::string Situacao;
      Curso* CursoAluno;
};

#endif // ALUNO_H
