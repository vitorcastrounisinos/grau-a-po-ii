#ifndef CURSO_H
#define CURSO_H
#include <vector>
#include "Professor.h"
#include "Aluno.h"
#include "Disciplina.h"
#include <iostream>
using namespace std;
class Aluno;
class Professor;
class Disciplina;
class Curso
{
    public:
        Curso();
        virtual ~Curso();
        Curso(std::string,int, std::string, int, std::string);

        void setCodigo(std::string codigo);
        std::string getCodigo();

        void setCargaHoraria(int cargaHoraria);
        int getCargaHoraria();

        void setDescricao(std::string descricao);
        std::string getDescricao();

        void setQuantidadePeriodos(int quantidadePeriodos);
        int getQuantidadePeriodos();

        void setTipoCurso(std::string tipoCurso);
        std::string getTipoCurso();

        void addProfessor(Professor* professor);
        Professor* getProfessor(int i);

        void addAluno(Aluno* aluno);
        Aluno* getAluno(int i);

    private:
        std::string Codigo;
        int CargaHoraria;
        std::string Descricao;
        int QuantidadePeriodos;
        std::string TipoCurso;
        std::vector<Professor*> Professores;
        std::vector<Aluno*> Alunos;
        std::vector<Disciplina*> Disciplinas;
};

#endif // CURSO_H
