#ifndef DISCIPLINA_H
#define DISCIPLINA_H
#include <vector>
#include "Turma.h"
#include "Professor.h"

class Turma;

class Disciplina
{
    public:
        Disciplina();
        virtual ~Disciplina();

        void setCodigo(std::string codigo);
        std::string getCodigo();

        void setPeriodo(std::string periodo);
        std::string getPeriodo();

        void setDescricao(std::string descricao);
        std::string getDescricao();

        void setNumeroAulas(int numeroAulas);
        int getNumeroAulas();

        void setEmenta(std::string ementa);
        std::string getEmenta();

        void setBibliografia(std::string bibliografia);
        std::string getBibliografia();

        void addTurma(Turma* turma);
        Turma* getTurma(int i);

    private:
        std::string Codigo;
        std::string Periodo;
        std::string Descricao;
        int NumeroAulas;
        std::string Ementa;
        std::string Bibliografia;
        std::vector<Turma*> Turmas;
};

#endif // DISCIPLINA_H
