#ifndef PESSOA_H
#define PESSOA_H
#include <string>

class Pessoa
{
    public:
        Pessoa();
        virtual ~Pessoa();
        Pessoa(std::string, std::string , std::string, std::string, std::string, std::string, std::string);

        void setPessoa(std::string, std::string , std::string, std::string, std::string, std::string, std::string);

        void setNome(std::string nome);
        std::string getNome();

        void setDataNascimento(std::string dataNascimento);
        std::string getDataNascimento();

        void setEmail(std::string email);
        std::string getEmail();

        void setEndereco(std::string endereco);
        std::string getEndereco();

        void setTelefone(std::string telefone);
        std::string getTelefone();

        void setCPF(std::string cpf);
        std::string getCPF();

        void setRG(std::string rg);
        std::string getRG();


   private:
        std::string Nome;
        std::string DataNascimento;
        std::string Email;
        std::string Endereco;
        std::string Telefone;
        std::string CPF;
        std::string RG;

};

#endif // PESSOA_H
