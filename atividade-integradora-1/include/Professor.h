#ifndef PROFESSOR_H
#define PROFESSOR_H
#include "Pessoa.h"
#include "Curso.h"
class Curso;

class Professor : public Pessoa
{
    public:
        Professor();
        virtual ~Professor();
        Professor(std::string , std::string, std::string, std::string, std::string, std::string, std::string, std::string ,
std::string, std::string, std::string );

        void setProfessor (std::string , std::string,
                  std::string, std::string, std::string, std::string, std::string, std::string ,
std::string, std::string, std::string );

        void setMatriculaProfessor(std::string matriculaProfessor);
        std::string getMatriculaProfessor();

        void setTitulacaoProfessor(std::string titulacaoProfessor);
        std::string getTitulacaoProfessor();

        void setTipoContrato(std::string tipoContrato);
        std::string getTipoContrato();

        void setBeneficios(std::string beneficios);
        std::string getBeneficios();


    private:
      std::string MatriculaProfessor;
      std::string TitulacaoProfessor;
      std::string TipoContrato;
      std::string Beneficios;
      std::string cadastrarProfessor;

};

#endif // PROFESSOR_H
