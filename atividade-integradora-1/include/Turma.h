#ifndef TURMA_H
#define TURMA_H
#include <vector>
#include "Aluno.h"
#include "Professor.h"
#include <iostream>
class Aluno;
class Professor;
class Turma
{
    public:
        Turma();
        virtual ~Turma();

        void setAno(std::string ano);
        std::string getAno();

        void setSemestre(std::string semestre);
        std::string getSemestre();

        void setDescricao(std::string descricao);
        std::string getDescricao();

        void setNumeroMaxAlunos(int numeroMaxAlunos);
        int getNumeroMaxAlunos();

        void setAlunos(Aluno aluno);
        Aluno getAluno(int i);

        void setProfessor(Professor* professor);
        Professor* getProfessor();

    private:
        std::string Ano;
        std::string Semestre;
        std::string Descricao;
        int NumeroMaxAlunos;
        std::vector<Aluno> Alunos;
        Professor* ProfessorTurma;
};

#endif // TURMA_H
