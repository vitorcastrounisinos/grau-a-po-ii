#include <iostream>
#include "Pessoa.h"
#include "Professor.h"
#include "Turma.h"
#include "Curso.h"
#include "Disciplina.h"
#include <vector>
#include <stdlib.h>

using namespace std;

int main()
{
    string nome_, nascimento_, email_, telefone_, endereco_, cpf_, rg_, mprof_, titprof_;
    string tipocontrato_, beneficios_, curso_, maluno_, aInicio_, sInicio_, situacao_, anoTurma_, semestre_, descricao_;
    string biblio_, ementa_, codigo_, tipoCurso_, periodo_, escolha;
    int cargah_, qtdPeriodos_, maxAluno_, numeroAulas_, i;

    vector<Professor> professoresCadastrados;
    vector<Aluno> alunosCadastrados;
    vector<Curso> cursosCadastrados;
    vector<Turma> turmasCadastradas;
    vector<Disciplina> disciplinasCadastradas;

    Professor professor;
    Aluno aluno;
    Turma turma;
    Disciplina disciplina;
    Curso curso;

    int opcao = 0;
    int temp, temp2 = 0;

    while(opcao != -1)
    {
        std::cout << "Digite a opcao desejada: " << std::endl;
        std::cout << "1 - Cadastrar Professor" << std::endl;
        std::cout << "2 - Cadastrar Aluno" << std::endl;
        std::cout << "3 - Cadastrar Curso" << std::endl;
        std::cout << "4 - Cadastrar Turma" << std::endl;
        std::cout << "5 - Cadastrar Disciplina" << std::endl;

        std::cout << "6 - Visualizar professores" << std::endl;
        std::cout << "7 - Visualizar alunos" << std::endl;
        std::cout << "8 - Visualizar turmas" << std::endl;
        std::cout << "9 - Visualizar cursos" << std::endl;
        std::cout << "10 - Visualizar disciplinas" << std::endl;

        std::cout << "11 - Matricular aluno a um curso" << std::endl;
        std::cout << "12 - Matricular aluno a uma turma" << std::endl;

        std::cout << "15 - Sair" << std::endl;

        std::cin >> opcao;
        system("cls");

        switch(opcao)
        {
            case 1: { //CADASTRO PROFESSOR
                std::cout << "Digite o nome: " << std::endl;
                std::cin >> nome_;
                professor.setNome(nome_);
                std::cout << "Digite a data de nascimento: " << std::endl;
                std::cin >> nascimento_;
                professor.setDataNascimento(nascimento_);
                std::cout << "Digite o email: " << std::endl;
                std::cin >> email_;
                professor.setEmail(email_);
                std::cout << "Digite o telefone: " << std::endl;
                std::cin >> telefone_;
                professor.setTelefone(telefone_);
                std::cout << "Digite o cpf: " << std::endl;
                std::cin >> cpf_;
                professor.setCPF(cpf_);
                std::cout << "Digite o RG: " << std::endl;
                std::cin >> rg_;
                professor.setRG(rg_);
                std::cout << "Digite a matricula: " << std::endl;
                std::cin >> mprof_;
                professor.setMatriculaProfessor(mprof_);
                 std::cout << "Digite a titulacao maxima: " << std::endl;
                std::cin >> titprof_;
                professor.setTitulacaoProfessor(titprof_);
                 std::cout << "Tipo contrato: " << std::endl;
                std::cin >> tipocontrato_;
                professor.setTipoContrato(tipocontrato_);
                std::cout << "Digite os beneficios: " << std::endl;
                std::cin >> beneficios_;
                professor.setBeneficios(beneficios_);
                std::cout<<"digite o curso o qual o professor tem aulas " <<std::endl;
                professoresCadastrados.push_back(professor);
                system("cls");
                std::cout << "Cadastrado!" << std::endl;
                break;
            }

            case 2: {//CADASTRO ALUNO
                std::cout << "Digite o nome: " << std::endl;
                std::cin >> nome_;
                aluno.setNome(nome_);
                std::cout << "Digite a data de nascimento: " << std::endl;
                std::cin >> nascimento_;
                aluno.setDataNascimento(nascimento_);
                std::cout << "Digite o email: " << std::endl;
                std::cin >> email_;
                aluno.setEmail(email_);
                std::cout << "Digite o telefone: " << std::endl;
                std::cin >> telefone_;
                aluno.setTelefone(telefone_);
                std::cout << "Digite o cpf: " << std::endl;
                std::cin >> cpf_;
                aluno.setCPF(cpf_);
                std::cout << "Digite o RG: " << std::endl;
                std::cin >> rg_;
                aluno.setRG(rg_);
                 std::cout << "Digite a matricula: " << std::endl;
                std::cin >> maluno_;
                aluno.setMatriculaAluno(maluno_);
                std::cout << "Digite o ano de inicio: " << std::endl;
                std::cin >> aInicio_;
                aluno.setAnoInicio(aInicio_);
                std::cout << "Semestre inicio: " << std::endl;
                std::cin >> sInicio_;
                aluno.setSemestreInicio(sInicio_);
                std::cout << "Situacao: " << std::endl;
                std::cin >> situacao_;
                aluno.setSituacao(situacao_);
                system("cls");
                std::cout << "Cadastrado!" << std::endl;
                break;
            }

            case 3: {//CURSO
                std::cout << "Digite o codigo: " << std::endl;
                std::cin >> codigo_;
                curso.setCodigo(codigo_);
                std::cout << "Digite a descricao: " << std::endl;
                std::cin >> descricao_;
                curso.setDescricao(descricao_);
                std::cout << "Digite a carga horaria: " << std::endl;
                std::cin >> cargah_;
                curso.setCargaHoraria(cargah_);
                std::cout << "Digite a quantidade de periodos: " << std::endl;
                std::cin >> qtdPeriodos_;
                curso.setQuantidadePeriodos(qtdPeriodos_);
                std::cout << "Tipo curso: " << std::endl;
                std::cin >> tipoCurso_;
                cursosCadastrados.push_back(curso);

                system("cls");
                std::cout << "Cadastrado!" << std::endl;
                break;
            }

            case 4: {//CADASTRAR TURMA
                std::cout << "Digite o ano: " << std::endl;
                std::cin >> anoTurma_;
                turma.setAno(anoTurma_);
                std::cout << "Digite o semestre: " << std::endl;
                std::cin >> semestre_;
                turma.setSemestre(semestre_);
                std::cout << "Digite a descricao: " << std::endl;
                std::cin >> descricao_;
                turma.setDescricao(descricao_);
                std::cout << "Digite o numero maximo de alunos: " << std::endl;
                std::cin >> maxAluno_;
                turma.setNumeroMaxAlunos(maxAluno_);

                std::cout << std::endl << "Escolha uma das disciplinas: " << std::endl;

                for(int i = 0; i < disciplinasCadastradas.size(); i++)
                {
                    std::cout << i + 1 << " - " << "Codigo: " << disciplinasCadastradas[i].getCodigo() << " - " << disciplinasCadastradas[i].getDescricao() << std::endl;
                }

                std::cout << std::endl << "Digite o numero da disciplina: " << std::endl;
                std::cin >> temp;

                if (disciplinasCadastradas.size() > temp) {
                  std::cout << std::endl << "Disciplina inexistente!" << std::endl;
                  break;
                }

                turmasCadastradas.push_back(turma);
                disciplinasCadastradas[temp - 1].addTurma(&turmasCadastradas[turmasCadastradas.size() - 1]);

                system("cls");
                std::cout << "Cadastrada!" << std::endl;
                break;
            }

            case 5: {//CADASTRAR DISCIPLINAS
                std::cout << "Digite o codigo da disciplina: " << std::endl;
                std::cin >> codigo_;
                disciplina.setCodigo(codigo_);
                std::cout << "Digite o periodo: " << std::endl;
                std::cin >> periodo_;
                disciplina.setPeriodo(periodo_);
                std::cout << "Digite a descricao: " << std::endl;
                std::cin >> descricao_;
                disciplina.setDescricao(descricao_);
                std::cout << "Digite numero aulas: " << std::endl;
                std::cin >> numeroAulas_;
                disciplina.setNumeroAulas(numeroAulas_);
                std::cout << "Digite a ementa: " << std::endl;
                std::cin >> ementa_;
                disciplina.setEmenta(ementa_);
                std::cout << "Digite a bibliografia: " << std::endl;
                std::cin >> biblio_;
                disciplina.setBibliografia(biblio_);
                system("cls");
                disciplinasCadastradas.push_back(disciplina);
                break;
            }

            case 6: {

                std::cout << std::endl << "Professores Cadastrados" << std::endl;

                for(int i = 0; i < professoresCadastrados.size(); i++)
                {
                    std::cout << "Nome: " << professoresCadastrados[i].getNome() << std::endl;
                    std::cout << "Data de nascimento: " << professoresCadastrados[i].getDataNascimento() << std::endl;
                    std::cout << "Email: " << professoresCadastrados[i].getEmail() << std::endl;
                    std::cout << "Telefone: " << professoresCadastrados[i].getTelefone() << std::endl;
                    std::cout << "Cpf: " << professoresCadastrados[i].getCPF() << std::endl;
                    std::cout << std::endl;
                }
                break;
            }

            case 7: {

                std::cout << std::endl << "Alunos Cadastrados" << std::endl;

                for(int i = 0; i < alunosCadastrados.size(); i++)
                {
                    std::cout << "Nome: " << alunosCadastrados[i].getNome() << std::endl;
                    std::cout << "Data de nascimento: " << alunosCadastrados[i].getDataNascimento() << std::endl;
                    std::cout << "Email: " << alunosCadastrados[i].getEmail() << std::endl;
                    std::cout << "Telefone: " << alunosCadastrados[i].getTelefone() << std::endl;
                    std::cout << "Cpf: " << alunosCadastrados[i].getCPF() << std::endl;
                    std::cout << std::endl;
                }
                break;
            }

            case 8: {

                std::cout << std::endl << "Turmas Cadastradas" << std::endl;

                for(int i = 0; i < turmasCadastradas.size(); i++)
                {
                    std::cout << "Ano: " << turmasCadastradas[i].getAno() << std::endl;
                    std::cout << "Semestre: " << turmasCadastradas[i].getSemestre() << std::endl;
                    std::cout << "Descricao: " << turmasCadastradas[i].getDescricao() << std::endl;
                    std::cout << "Numero maximo de alunos: " << turmasCadastradas[i].getNumeroMaxAlunos() << std::endl;
                    std::cout << std::endl;
                }
                break;
            }

            case 9: {

                std::cout << std::endl << "Cursos Cadastrados" << std::endl;

                for(int i = 0; i < cursosCadastrados.size(); i++)
                {
                    std::cout << "Codigo: " << cursosCadastrados[i].getCodigo() << std::endl;
                    std::cout << "Descricao: " << cursosCadastrados[i].getDescricao() << std::endl;
                    std::cout << "Quantidade de periodos: " << cursosCadastrados[i].getQuantidadePeriodos() << std::endl;
                    std::cout << "Tipo do Curso: " << cursosCadastrados[i].getTipoCurso() << std::endl;
                    std::cout << std::endl;
                }
                break;
            }

            case 10: {

                std::cout << std::endl << "Disciplinas Cadastradas" << std::endl;

                for(int i = 0; i < disciplinasCadastradas.size(); i++)
                {
                    std::cout << "Codigo: " << disciplinasCadastradas[i].getCodigo() << std::endl;
                    std::cout << "Descricao: " << disciplinasCadastradas[i].getDescricao() << std::endl;
                    std::cout << "Ementa: " << disciplinasCadastradas[i].getEmenta() << std::endl;
                    std::cout << "Bibliografia: " << disciplinasCadastradas[i].getBibliografia() << std::endl;
                    std::cout << std::endl;
                }
                break;
            }

            case 11: {

                if (cursosCadastrados.size() < 1) {
                  std::cout << std::endl << "Não existem cursos cadastrados!" << std::endl;
                  break;
                }

                std::cout << std::endl << "Escolha um dos cursos: " << std::endl;

                for(int i = 0; i < cursosCadastrados.size(); i++)
                {
                    std::cout << i + 1 << " - " << "Codigo: " << cursosCadastrados[i].getCodigo() << " - " << cursosCadastrados[i].getDescricao() << std::endl;
                }

                std::cout << std::endl << "Digite o numero do curso: " << std::endl;
                std::cin >> temp;

                if (cursosCadastrados.size() > temp) {
                  std::cout << std::endl << "Curso inexistente!" << std::endl;
                  break;
                }

                std::cout << std::endl << "Escolha um dos alunos: " << std::endl;

                for(int i = 0; i < alunosCadastrados.size(); i++)
                {
                    std::cout << i + 1 << " - " << "Matricula: " << alunosCadastrados[i].getMatriculaAluno() << " - Nome: " << alunosCadastrados[i].getNome() << std::endl;
                }

                std::cout << std::endl << "Digite o numero do aluno: " << std::endl;
                std::cin >> temp2;

                if (alunosCadastrados.size() > temp2) {
                  std::cout << std::endl << "Aluno inexistente!" << std::endl;
                  break;
                }

                cursosCadastrados[temp - 1].addAluno(&alunosCadastrados[temp2 - 1]);
                std::cout << std::endl << "Realizado!" << std::endl;

                break;
            }

            case 12: {

              /*std::cout << std::endl << "Escolha um dos alunos: " << std::endl;

              for(int i = 0; i < alunosCadastrados.size(); i++)
              {
                  std::cout << i + 1 << " - " << "Matricula: " << alunosCadastrados[i].getMatriculaAluno() << " - Nome: " << alunosCadastrados[i].getNome() << std::endl;
              }

              std::cout << std::endl << "Digite o numero do aluno: " << std::endl;
              std::cin >> temp2;

              if (alunosCadastrados.size() > temp2) {
                std::cout << std::endl << "Aluno inexistente!" << std::endl;
                break;
              }

              if(!alunosCadastrados[temp2-1].getCurso()) {
                std::cout << std::endl << "O aluno nao esta matriculado em nenhum curso!" << std::endl;
              }

              std::cout << std::endl << "Escolha uma das turmas: " << std::endl;

              for(int i = 0; i < alunosCadastrados[temp2-1].getCurso()->getDi; i++)
              {
                  std::cout << i + 1 << " - " << "Codigo: " << cursosCadastrados[i].getCodigo() << " - " << cursosCadastrados[i].getDescricao() << std::endl;
              }

              std::cout << std::endl << "Digite o numero do curso: " << std::endl;
              std::cin >> temp;

              if (cursosCadastrados.size() > temp) {
                std::cout << std::endl << "Curso inexistente!" << std::endl;
                break;
              }*/



              break;
            }

            case 13: {
                opcao = -1;
                break;
            }
        }
    }

    return 0;
}
