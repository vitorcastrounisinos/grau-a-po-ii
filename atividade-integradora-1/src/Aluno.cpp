#include "Aluno.h"
#include "Pessoa.h"
Aluno::Aluno()
{
    //ctor
}

Aluno::~Aluno()
{
    //dtor
}
Aluno::Aluno(std::string matriculaAluno, std::string anoInicio, std::string semestreInicio, std::string situacao,
             Curso* curso, std::string nome, std::string dataNascimento, std::string email, std::string endereco, std::string telefone, std::string cpf, std::string rg)
{

    setAluno(matriculaAluno, anoInicio, semestreInicio, situacao, curso, nome, dataNascimento, email, endereco, telefone, cpf, rg);
}

void Aluno::setAluno(std::string matriculaAluno, std::string anoInicio, std::string semestreInicio, std::string situacao,
             Curso* curso, std::string nome, std::string dataNascimento, std::string email, std::string endereco, std::string telefone, std::string cpf, std::string rg)
{
    setNome(nome);
    setDataNascimento(dataNascimento);
    setEmail(email);
    setEndereco(endereco);
    setTelefone(telefone);
    setCPF(cpf);
    setRG(rg);
    setMatriculaAluno(MatriculaAluno);
    setAnoInicio(AnoInicio);
    setSemestreInicio(SemestreInicio);
    setSituacao(Situacao);
    setCurso(curso);
}
void Aluno::setMatriculaAluno(std::string matriculaAluno)
{
    MatriculaAluno = matriculaAluno;
}

std::string Aluno::getMatriculaAluno()
{
    return MatriculaAluno;
}

void Aluno::setAnoInicio(std::string anoInicio)
{
    AnoInicio = anoInicio;
}

std::string Aluno::getAnoInicio()
{
    return AnoInicio;
}

void Aluno::setSemestreInicio(std::string semestreInicio)
{
    SemestreInicio = semestreInicio;
}

std::string Aluno::getSemestreInicio()
{
    return SemestreInicio;
}

void Aluno::setSituacao(std::string situacao)
{
    Situacao = situacao;
}

std::string Aluno::getSituacao()
{
    return Situacao;
}

void Aluno::setCurso(Curso* curso)
{
    CursoAluno = curso;
}

Curso* Aluno::getCurso()
{
    return CursoAluno;
}
