#include "Curso.h"

Curso::Curso()
{
    //ctor
}

Curso::~Curso()
{
    //dtor
}

void Curso::setCodigo(std::string codigo)
{
    Codigo = codigo;
}

Curso::Curso(std::string codigo,int cargaHoraria, std::string descricao, int quantidadePeriodos, std::string  tipoCurso)
{
    setCodigo(codigo);
    setCargaHoraria(cargaHoraria);
    setDescricao(descricao);
    setQuantidadePeriodos(quantidadePeriodos);
    setTipoCurso(tipoCurso);
}


std::string Curso::getCodigo()
{
    return Codigo;
}

void Curso::setCargaHoraria(int cargaHoraria)
{
    CargaHoraria = cargaHoraria;
}

int Curso::getCargaHoraria()
{
    return CargaHoraria;
}

void Curso::setDescricao(std::string descricao)
{
    Descricao = descricao;
}

std::string Curso::getDescricao()
{
    return Descricao;
}

void Curso::setQuantidadePeriodos(int quantidadePeriodos)
{
    QuantidadePeriodos = quantidadePeriodos;
}

int Curso::getQuantidadePeriodos()
{
    return QuantidadePeriodos;
}

void Curso::setTipoCurso(std::string tipoCurso)
{
    TipoCurso = tipoCurso;
}

std::string Curso::getTipoCurso()
{
    return TipoCurso;
}

void Curso::addProfessor(Professor* professor)
{
    Professores.push_back(professor);
}

Professor* Curso::getProfessor(int i)
{
    return Professores[i];
}

void Curso::addAluno(Aluno* aluno)
{
    Alunos.push_back(aluno);
}

Aluno* Curso::getAluno(int i)
{
    return Alunos[i];
}
