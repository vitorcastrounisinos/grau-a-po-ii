#include "Disciplina.h"

Disciplina::Disciplina()
{
    //ctor
}

Disciplina::~Disciplina()
{
    //dtor
}

void Disciplina::setCodigo(std::string codigo)
{
    Codigo = codigo;
}

std::string Disciplina::getCodigo()
{
    return Codigo;
}

void Disciplina::setPeriodo(std::string periodo)
{
    Periodo = periodo;
}

std::string Disciplina::getPeriodo()
{
    return Periodo;
}

void Disciplina::setDescricao(std::string descricao)
{
    Descricao = descricao;
}

std::string Disciplina::getDescricao()
{
    return Descricao;
}

void Disciplina::setNumeroAulas(int numeroAulas)
{
    NumeroAulas = numeroAulas;
}

int Disciplina::getNumeroAulas()
{
    return NumeroAulas;
}

void Disciplina::setEmenta(std::string ementa)
{
    Ementa = ementa;
}

std::string Disciplina::getEmenta()
{
    return Ementa;
}

void Disciplina::setBibliografia(std::string bibliografia)
{
    Bibliografia = bibliografia;
}

std::string Disciplina::getBibliografia()
{
    return Bibliografia;
}

void Disciplina::addTurma(Turma* turma)
{
    Turmas.push_back(turma);
}

Turma* Disciplina::getTurma(int i)
{
    return Turmas[i];
}
