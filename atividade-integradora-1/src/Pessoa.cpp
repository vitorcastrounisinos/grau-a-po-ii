#include "Pessoa.h"
#include "Professor.h"
Pessoa::Pessoa()
{
    //ctor
}

Pessoa::~Pessoa()
{
    //dtor
}
Pessoa::Pessoa(std::string nome, std::string dataNascimento, std::string email, std::string endereco, std::string telefone, std::string cpf, std::string rg)
{
    setPessoa(nome, dataNascimento, email, endereco, telefone, cpf, rg);
}
void Pessoa::setPessoa(std::string nome, std::string dataNascimento, std::string email, std::string endereco, std::string telefone, std::string cpf, std::string rg)
{
    setNome(nome);
    setDataNascimento(dataNascimento);
    setEmail(email);
    setEndereco(endereco);
    setTelefone(telefone);
    setCPF(cpf);
    setRG(rg);
}
void Pessoa::setNome(std::string nome)
{
    Nome = nome;
}

std::string Pessoa::getNome()
{
    return Nome;
}

void Pessoa::setDataNascimento(std::string dataNascimento)
{
    DataNascimento = dataNascimento;
}

std::string Pessoa::getDataNascimento()
{
    return DataNascimento;
}

void Pessoa::setEmail(std::string email)
{
    Email = email;
}

std::string Pessoa::getEmail()
{
    return Email;
}

void Pessoa::setEndereco(std::string endereco)
{
    Endereco = endereco;
}

std::string Pessoa::getEndereco()
{
    return Endereco;
}

void Pessoa::setTelefone(std::string telefone)
{
    Telefone = telefone;
}

std::string Pessoa::getTelefone()
{
    return Telefone;
}

void Pessoa::setCPF(std::string cpf)
{
    CPF = cpf;
}

std::string Pessoa::getCPF()
{
    return CPF;
}

void Pessoa::setRG(std::string rg)
{
    RG = rg;
}

std::string Pessoa::getRG()
{
    return RG;
}
