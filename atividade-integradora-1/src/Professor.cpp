#include "Professor.h"
#include "Pessoa.h"
Professor::Professor()
{
    //ctor
}

Professor::~Professor()
{
    //dtor
}
Professor::Professor(std::string matriculaProfessor, std::string titulacaoProfessor, std::string tipoContrato,
std::string beneficios, std::string nome, std::string dataNascimento, std::string email, std::string endereco,
std::string telefone, std::string cpf, std::string rg)
{

    setNome(nome);
    setDataNascimento(dataNascimento);
    setEmail(email);
    setEndereco(endereco);
    setTelefone(telefone);
    setCPF(cpf);
    setRG(rg);
    setMatriculaProfessor(matriculaProfessor);
    setTitulacaoProfessor(titulacaoProfessor);
    setTipoContrato(tipoContrato);
    setBeneficios(beneficios);


}

void Professor::setMatriculaProfessor(std::string matriculaProfessor)
{
    MatriculaProfessor = matriculaProfessor;
}

std::string Professor::getMatriculaProfessor()
{
    return MatriculaProfessor;
}

void Professor::setTitulacaoProfessor(std::string titulacaoProfessor)
{
    TitulacaoProfessor = titulacaoProfessor;
}

std::string Professor::getTitulacaoProfessor()
{
    return TitulacaoProfessor;
}

void Professor::setTipoContrato(std::string tipoContrato)
{
    TipoContrato = tipoContrato;
}

std::string Professor::getTipoContrato()
{
    return TipoContrato;
}

void Professor::setBeneficios(std::string beneficios)
{
    Beneficios = beneficios;
}

std::string Professor::getBeneficios()
{
    return Beneficios;
}
