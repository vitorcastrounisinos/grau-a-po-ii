#include "Turma.h"

Turma::Turma()
{
    //ctor
}

Turma::~Turma()
{
    //dtor
}

void Turma::setAno(std::string ano)
{
    Ano = ano;
}

std::string Turma::getAno()
{
    return Ano;
}

void Turma::setSemestre(std::string semestre)
{
    Semestre = semestre;
}
std::string Turma::getSemestre()
{
    return Semestre;
}

void Turma::setDescricao(std::string descricao)
{
    Descricao = descricao;
}

std::string Turma::getDescricao()
{
    return Descricao;
}

void Turma::setNumeroMaxAlunos(int numeroMaxAlunos)
{
    NumeroMaxAlunos = numeroMaxAlunos;
}

int Turma::getNumeroMaxAlunos()
{
    return NumeroMaxAlunos;
}

void Turma::setAlunos(Aluno aluno)
{
    if(Alunos.size() <= NumeroMaxAlunos)
        Alunos.push_back(aluno);
}

Aluno Turma::getAluno(int i)
{
    return Alunos[i];
}

void Turma::setProfessor(Professor* professor)
{
    ProfessorTurma = professor;
}

Professor* Turma::getProfessor()
{
    return ProfessorTurma;
}
