#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QStringList titles;
    ui->tableWidget->setColumnCount(2);
    titles.push_back("Horario");
    titles.push_back("Valor");
    ui->tableWidget->setHorizontalHeaderLabels(titles);
    plotgrafico2();

    plotgrafico3();
    ui->qCustomPlot3->addGraph();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::plotgrafico2(void)
{
    Singleton* singleton = Singleton::getInstance();

    //layout
    QLinearGradient gradient(0, 0, 0, 400);
    gradient.setColorAt(0, QColor(90, 90, 90));
    gradient.setColorAt(0.38, QColor(105, 105, 105));
    gradient.setColorAt(1, QColor(70, 70, 70));
    ui->qCustomPlot2->setBackground(QBrush(gradient));

    QCPBars *values = new QCPBars(ui->qCustomPlot2->xAxis, ui->qCustomPlot2->yAxis);

    values->setName("Reais");
    values->setPen(QPen(QColor(111, 9, 176).lighter(170)));
    values->setBrush(QColor(111, 9, 176));

    QVector<double> ticks;
    QVector<QString> labels;
    QVector<double> valuesData;
    int size = singleton->getPrices()->size();
    QVector<Price> * prices = singleton->getPrices();
    for(int i = 0; i < size; i++)
    {
        Price p = prices->at(i);
        ticks << i + 1;
        valuesData << p.getValue();
        labels << p.getDate().toString();
    }

    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    textTicker->addTicks(ticks, labels);
    ui->qCustomPlot2->xAxis->setTicker(textTicker);
    ui->qCustomPlot2->xAxis->setTickLabelRotation(60);
    ui->qCustomPlot2->xAxis->setSubTicks(false);
    ui->qCustomPlot2->xAxis->setTickLength(0, 4);
    ui->qCustomPlot2->xAxis->setRange(0, 20);
    ui->qCustomPlot2->xAxis->setBasePen(QPen(Qt::white));
    ui->qCustomPlot2->xAxis->setTickPen(QPen(Qt::white));
    ui->qCustomPlot2->xAxis->grid()->setVisible(true);
    ui->qCustomPlot2->xAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));
    ui->qCustomPlot2->xAxis->setTickLabelColor(Qt::white);

    // prepare y axis:
    ui->qCustomPlot2->yAxis->setRange(60000.0, 62000.0);
    ui->qCustomPlot2->yAxis->setPadding(5); // a bit more space to the left border
    ui->qCustomPlot2->yAxis->setLabel("Bitcoin em Reais");
    ui->qCustomPlot2->yAxis->setBasePen(QPen(Qt::white));
    ui->qCustomPlot2->yAxis->setTickPen(QPen(Qt::white));
    ui->qCustomPlot2->yAxis->setSubTickPen(QPen(Qt::white));
    ui->qCustomPlot2->yAxis->grid()->setSubGridVisible(true);
    ui->qCustomPlot2->yAxis->setTickLabelColor(Qt::white);
    ui->qCustomPlot2->yAxis->setLabelColor(Qt::white);
    ui->qCustomPlot2->yAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::SolidLine));
    ui->qCustomPlot2->yAxis->grid()->setSubGridPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));

    values->setData(ticks, valuesData);
    ui->qCustomPlot2->replot();
}

void MainWindow::plotgrafico3(void)
{
    //layout
    QLinearGradient gradient(0, 0, 0, 400);
    gradient.setColorAt(0, QColor(90, 90, 90));
    gradient.setColorAt(0.38, QColor(105, 105, 105));
    gradient.setColorAt(1, QColor(70, 70, 70));
    ui->qCustomPlot3->setBackground(QBrush(gradient));

    ui->qCustomPlot3->yAxis->setTickLabelColor(Qt::white);
    ui->qCustomPlot3->yAxis->setLabelColor(Qt::white);

    ui->qCustomPlot3->xAxis->setTickLabelColor(Qt::white);

    Singleton* singleton = Singleton::getInstance();

    int size = singleton->getPrices()->size();
    QVector<Price> * prices = singleton->getPrices();


    if(size >= 1) {
        // create multiple graphs:
        for (int gi=0; gi<1; ++gi)
        {
          QColor color(20+200/4.0*gi,70*(1.6-gi/4.0), 150, 150);
          ui->qCustomPlot3->graph()->setLineStyle(QCPGraph::lsLine);
          ui->qCustomPlot3->graph()->setPen(QPen(color.lighter(200)));
          ui->qCustomPlot3->graph()->setBrush(QBrush(color));

          // generate random walk data:
          QVector<QCPGraphData> timeData(250);
          for (int i=0; i < size; ++i)
          {
            Price p = prices->at(i);
            timeData[i].key = p.getDate().toTime_t();
            timeData[i].value = p.getValue();
          }
          ui->qCustomPlot3->graph()->data()->set(timeData);
        }
    }
    // configure bottom axis to show date instead of number:
    QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
    ui->qCustomPlot3->xAxis->setTicker(dateTicker);

    // set axis labels:
    ui->qCustomPlot3->yAxis->setLabel("Bitcoin em Reais");

    // make top and right axes visible but without ticks and labels:
    ui->qCustomPlot3->xAxis2->setVisible(true);
    ui->qCustomPlot3->yAxis2->setVisible(true);
    ui->qCustomPlot3->xAxis2->setTicks(false);
    ui->qCustomPlot3->yAxis2->setTicks(false);
    ui->qCustomPlot3->xAxis2->setTickLabels(false);
    ui->qCustomPlot3->yAxis2->setTickLabels(false);

    // set axis ranges to show all data:
    if (size > 0) {
        Price price = prices->at(0);
        Price price2 = prices->last();
        ui->qCustomPlot3->xAxis->setRange(price.getDate().toTime_t(), price2.getDate().toTime_t());
    }

    ui->qCustomPlot3->yAxis->setRange(60000.0, 62000.0);
    // show legend with slightly transparent background brush:
    ui->qCustomPlot3->legend->setVisible(true);
    ui->qCustomPlot3->legend->setBrush(QColor(255, 255, 255, 150));
    ui->qCustomPlot3->replot();
}


void MainWindow::on_pushButton_clicked()
{
    Singleton* singleton = Singleton::getInstance();

    //format
    QString format = QString("yyyy-MM-dd hh:mm:ss");
    QString horario = ui->lineEdit->text();
    QString valor = ui->lineEdit2->text();
    valor.replace(R"(,)", R"(.)");
    valor = valor.simplified();
    horario = horario.simplified();

    //storage
    QDateTime date = QDateTime::fromString(horario, format);
    double value = valor.toDouble();
    singleton->addPrice(date, value);

    Price lastPriceAdded = singleton->getPrices()->last();

    //tableWidget
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    int line = ui->tableWidget->rowCount() - 1;
    ui->tableWidget->setItem(line, 0, new QTableWidgetItem(lastPriceAdded.getDate().toString()));
    ui->tableWidget->setItem(line, 1, new QTableWidgetItem(QString::number(lastPriceAdded.getValue(), 'f', 6)));

    //plots
    plotgrafico2();
    plotgrafico3();
}

void MainWindow::carregarTabela(void)
{
    Singleton* singleton = Singleton::getInstance();
    QVector<Price>* prices = singleton->getPrices();
    int count = prices->size() - 10;
    while(count < prices->size()) {
        Price lastPriceAdded = prices->at(count);
        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
        int line = ui->tableWidget->rowCount() - 1;
        ui->tableWidget->setItem(line, 0, new QTableWidgetItem(lastPriceAdded.getDate().toString()));
        ui->tableWidget->setItem(line, 1, new QTableWidgetItem(QString::number(lastPriceAdded.getValue(), 'f', 6)));
        count++;
    }


}

void MainWindow::on_pushButton_2_clicked()
{
    Singleton* singleton = Singleton::getInstance();
    QString format = QString("yyyy-MM-dd hh:mm:ss");

    singleton->addPrice(QDateTime::fromString("2020-09-30 13:09:00", format), 61409.31821);
    singleton->addPrice(QDateTime::fromString("2020-09-30 14:09:00", format), 61398.61217);
    singleton->addPrice(QDateTime::fromString("2020-09-30 15:09:00", format), 60916.95202);
    singleton->addPrice(QDateTime::fromString("2020-09-30 16:09:00", format), 60837.22411);
    singleton->addPrice(QDateTime::fromString("2020-09-30 17:09:00", format), 61013.57756);
    singleton->addPrice(QDateTime::fromString("2020-09-30 18:09:00", format), 60819.365);
    singleton->addPrice(QDateTime::fromString("2020-09-30 19:09:00", format), 60929.2);
    singleton->addPrice(QDateTime::fromString("2020-09-30 20:09:00", format), 61055.67336);
    singleton->addPrice(QDateTime::fromString("2020-09-30 21:09:00", format), 60962.98835);
    singleton->addPrice(QDateTime::fromString("2020-09-30 22:09:00", format), 60949.33871);
    plotgrafico2();
    plotgrafico3();
    carregarTabela();
}
