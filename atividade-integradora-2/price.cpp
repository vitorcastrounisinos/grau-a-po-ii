#include "price.h"

Price::Price(QDateTime d, double v)
{
    setDate(d);
    setValue(v);
}

QDateTime Price::getDate()
{
    return date;
}

void Price::setDate(QDateTime d)
{
    date = d;
}

double Price::getValue()
{
    return value;
}

void Price::setValue(double v)
{
    value = v;
}

