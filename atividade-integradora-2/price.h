#ifndef PRICE_H
#define PRICE_H
#include <QtCore>

class Price
{
public:
    Price(QDateTime d, double v);

    QDateTime getDate();
    void setDate(QDateTime d);

    double getValue();
    void setValue(double v);

private:
    QDateTime date;
    double value;
};

#endif // PRICE_H
