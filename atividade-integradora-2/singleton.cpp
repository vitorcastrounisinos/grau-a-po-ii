#include "singleton.h"

Singleton* Singleton::instance = 0;

Singleton* Singleton::getInstance()
{
    if(instance == 0)
    {
        instance = new Singleton();
    }

    return instance;
}

Singleton::Singleton()
{

}

QVector<Price>* Singleton::getPrices()
{
    return &prices;
}

void Singleton::addPrice(QDateTime date, double value)
{
    Price price = Price(date, value);
    prices.push_back(price);
}
