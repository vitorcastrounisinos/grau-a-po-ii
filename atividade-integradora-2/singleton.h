#ifndef SINGLETON_H
#define SINGLETON_H
#include <QVector>
#include <QDate>
#include "price.h"

class Singleton
{
    private:
        static Singleton* instance;

        Singleton();

        QVector<Price> prices;

    public:
        static Singleton* getInstance();

        QVector<Price>* getPrices();
        void addPrice(QDateTime date, double value);
};


#endif // SINGLETON_H
