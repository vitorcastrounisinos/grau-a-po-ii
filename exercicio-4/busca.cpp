#include "busca.h"
#include "ui_busca.h"
#include <QTextEdit>
#include <QString>
Busca::Busca(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Busca)
{
    ui->setupUi(this);
     ocorrencia=0;
     caseSensitive = true;
}

Busca::~Busca()
{
    delete ui;
}

void Busca::on_AtivarDesativar_clicked()
{
    caseSensitive = !caseSensitive;

    if(caseSensitive) {
        ui->AtivarDesativar->setText("Desativar");
    }
    else {
        ui->AtivarDesativar->setText("Ativar");
    }
}

void Busca::on_Pesquisa_clicked()
{
    QString texto = ui->textEdit->toPlainText();
    QString busca = ui->lineEditBusca->text();

        if(caseSensitive)
        {
            ocorrencia = texto.count(busca, Qt::CaseSensitive);
            texto.replace(busca, QString("<font color=\"#FF0000\">" + busca + "</font>"), Qt::CaseSensitive);
        }
        else {
            ocorrencia = texto.count(busca, Qt::CaseInsensitive);
            texto.replace(busca, QString("<font color=\"#FF0000\">" + busca + "</font>"), Qt::CaseInsensitive);
        }

     ui->textEdit->setText(texto);
     ui->lineEditQuantidade->setText(QString("%1").arg(ocorrencia));
}




void Busca::on_Limpar_clicked()
{
    ui->lineEditBusca->clear();
    ui->lineEditQuantidade->clear();
    ocorrencia=0;
}

void Busca::on_Limpar_2_clicked()
{
    ui->textEdit->clear();
}
