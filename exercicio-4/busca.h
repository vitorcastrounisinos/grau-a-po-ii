#ifndef BUSCA_H
#define BUSCA_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Busca; }
QT_END_NAMESPACE

class Busca : public QMainWindow
{
    Q_OBJECT

public:
    Busca(QWidget *parent = nullptr);
    ~Busca();

private slots:
    void on_AtivarDesativar_clicked();

    void on_Pesquisa_clicked();

    void on_Limpar_clicked();

    void on_Limpar_2_clicked();

private:
    Ui::Busca *ui;
     void atualizaTextEdit();
     int ocorrencia;
     bool caseSensitive;

};
#endif // BUSCA_H
