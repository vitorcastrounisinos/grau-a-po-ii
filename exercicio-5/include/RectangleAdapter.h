#ifndef RECTANGLEADAPTER_H
#define RECTANGLEADAPTER_H

#include "LegacyRectangle.h"
#include "Rectangle.h"

class RectangleAdapter : public Rectangle
{
    public:
        RectangleAdapter(int x, int y, int w, int h);
        virtual ~RectangleAdapter();
        void draw();

    private:
        int x;
        int y;
        int w;
        int h;
};

#endif // RECTANGLEADAPTER_H
