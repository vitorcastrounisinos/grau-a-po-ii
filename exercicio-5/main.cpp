#include <iostream>
#include "Rectangle.h"
#include "RectangleAdapter.h"

using namespace std;

int main()
{
    int x = 20, y = 50, w = 300, h = 200;
    Rectangle *r = new RectangleAdapter(x, y, w, h);
    r->draw();

    return 0;
}
