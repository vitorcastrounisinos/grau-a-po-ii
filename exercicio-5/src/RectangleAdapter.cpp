#include "RectangleAdapter.h"

RectangleAdapter::RectangleAdapter(int x, int y, int w, int h)
{
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
}

RectangleAdapter::~RectangleAdapter()
{

}

void RectangleAdapter::draw()
{
    std::cout << "RectangleAdapter: draw. \n";
    LegacyRectangle *legacyRectangle = new LegacyRectangle(x, y, x + w, y + h);
    legacyRectangle->oldDraw();
    delete legacyRectangle;
}
