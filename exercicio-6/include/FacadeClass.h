#ifndef FACADECLASS_H
#define FACADECLASS_H

#include <Maximo.h>
#include <Minimo.h>
#include <Media.h>

class FacadeClass
{
    public:
        FacadeClass(float value[10]);
        virtual ~FacadeClass();
        void calcular();

    private:
        Media media;
        Minimo minimo;
        Maximo maximo;
        float* value;
};

#endif // FACADECLASS_H
