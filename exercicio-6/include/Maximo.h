#ifndef MAXIMO_H
#define MAXIMO_H

#include <iostream>

class Maximo
{
    public:
        Maximo();
        virtual ~Maximo();
        void calcular(float value[10]);

};

#endif // MAXIMO_H
