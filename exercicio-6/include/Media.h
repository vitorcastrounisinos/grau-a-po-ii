#ifndef MEDIA_H
#define MEDIA_H

#include <iostream>

class Media
{
    public:
        Media();
        virtual ~Media();
        void calcular(float value[10]);

};

#endif // MEDIA_H
