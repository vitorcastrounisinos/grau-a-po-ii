#ifndef MINIMO_H
#define MINIMO_H

#include <iostream>

class Minimo
{
    public:
        Minimo();
        virtual ~Minimo();
        void calcular(float value[10]);
};

#endif // MINIMO_H
