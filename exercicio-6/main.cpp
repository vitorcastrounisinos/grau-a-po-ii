#include <iostream>
#include <FacadeClass.h>

using namespace std;

int main()
{
    float vetor[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    FacadeClass facadeClass(vetor);
    facadeClass.calcular();

    return 0;
}
