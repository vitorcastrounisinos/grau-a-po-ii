#include "FacadeClass.h"

FacadeClass::FacadeClass(float value[10])
{
    this->value = value;
    media = Media();
    minimo = Minimo();
    maximo = Maximo();
    //ctor
}

FacadeClass::~FacadeClass()
{
    //dtor
}

void FacadeClass::calcular()
{
    media.calcular(value);
    minimo.calcular(value);
    maximo.calcular(value);
}
