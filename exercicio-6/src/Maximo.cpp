#include "Maximo.h"

Maximo::Maximo()
{
    //ctor
}

Maximo::~Maximo()
{
    //dtor
}

void Maximo::calcular(float value[10])
{
    float maximo = value[0];
    for(int i = 0; i < 10; i++)
        if(value[i] > maximo)
            maximo = value[i];

    std::cout << "Maximo: " << maximo << std::endl;
}
