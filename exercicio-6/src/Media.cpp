#include "Media.h"

Media::Media()
{
    //ctor
}

Media::~Media()
{
    //dtor
}

void Media::calcular(float value[10])
{
    float soma = 0;
    for(int i = 0; i < 10; i++)
        soma += value[i];

    std::cout << "Media: " << soma/10.0 << std::endl;
}
