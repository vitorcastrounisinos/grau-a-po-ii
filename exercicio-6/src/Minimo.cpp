#include "Minimo.h"

Minimo::Minimo()
{
    //ctor
}

Minimo::~Minimo()
{
    //dtor
}

void Minimo::calcular(float value[10])
{
    float minimo = value[0];
    for(int i = 0; i < 10; i++)
        if(value[i] < minimo)
            minimo = value[i];

    std::cout << "Minimo: " << minimo << std::endl;
}
