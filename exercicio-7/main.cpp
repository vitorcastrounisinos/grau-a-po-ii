#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>

class TimeImp {
  public:
    TimeImp(int hr, int min) {
        hr_ = hr;
        min_ = min;
    }
    virtual void tell() {
        std::cout << "time is " << std::setw(2) << std::setfill(' ') << hr_ << min_ << std::endl;
    }
  protected:
    int hr_, min_;
};

class CivilianTimeImp: public TimeImp {
  public:
    CivilianTimeImp(int hr, int min, int pm): TimeImp(hr, min) {
        if (pm)
          strcpy(whichM_, " PM");
        else
          strcpy(whichM_, " AM");
    }

    /* virtual */
    void tell() {
        std::cout << "time is " << hr_ << ":" << min_ << whichM_ << std::endl;
    }
  protected:
    char whichM_[4];
};

class ZuluTimeImp: public TimeImp {
  public:
    ZuluTimeImp(int hr, int min, int zone): TimeImp(hr, min) {
        if (zone == 5)
          strcpy(zone_, " Eastern Standard Time");
        else if (zone == 6)
          strcpy(zone_, " Central Standard Time");
    }

    /* virtual */
    void tell() {
        std::cout << "time is " << std::setw(2) << hr_ << min_ << zone_ << std::endl;
    }
  protected:
    char zone_[30];
};

class BrasilTimeImp: public TimeImp {
  public:
    BrasilTimeImp(int hr, int min, int zone): TimeImp(hr, min) {
        if (zone == -5)
          strcpy(zone_, " ACT - Acre Time");
        else if (zone == -4)
          strcpy(zone_, " AMT - Amazon (Standard) Time");
        else if (zone == -3)
          strcpy(zone_, " BRT - Brasilia (Standard) Time");
        else if (zone == -2)
          strcpy(zone_, " FNT - Fernando de Noronha Archipelago Time");
    }

    /* virtual */
    void tell() {
        std::cout << "time is " << std::setw(2) << hr_ <<  ":" << min_ << zone_ << std::endl;
    }
  protected:
    char zone_[30];
};

class Time {
  public:
    Time(){}
    Time(int hr, int min) {
        imp_ = new TimeImp(hr, min);
    }
    virtual void tell() {
        imp_->tell();
    }
  protected:
    TimeImp *imp_;
};

class CivilianTime: public Time {
  public:
    CivilianTime(int hr, int min, int pm) {
        imp_ = new CivilianTimeImp(hr, min, pm);
    }
};

class ZuluTime: public Time {
  public:
    ZuluTime(int hr, int min, int zone) {
        imp_ = new ZuluTimeImp(hr, min, zone);
    }
};

class BrasilTime: public Time {
  public:
    BrasilTime(int hr, int min, int zone) {
        imp_ = new BrasilTimeImp(hr, min, zone);
    }
};

int main() {
  Time *times[4];
  times[0] = new Time(14, 30);
  times[1] = new CivilianTime(2, 30, 1);
  times[2] = new ZuluTime(14, 30, 6);
  times[3] = new BrasilTime(14, 30, -3);

  for (int i = 0; i < 4; i++)
    times[i]->tell();
}
