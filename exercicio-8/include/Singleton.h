#ifndef SINGLETON_H
#define SINGLETON_H


class Singleton
{
    private:
        static Singleton* instance;

        Singleton();

    public:
        static Singleton* getInstance();
};

#endif // SINGLETON_H
