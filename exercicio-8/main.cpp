#include <iostream>
#include "Singleton.h"

int main()
{
    Singleton* s = Singleton::getInstance();
    Singleton* r = Singleton::getInstance();

    std::cout << s << std::endl;
    std::cout << r << std::endl;

    delete s;

    std::cout << s << std::endl;
    std::cout << r << std::endl;

    delete r;

    std::cout << s << std::endl;
    std::cout << r << std::endl;

    //deleta conteudo da instancia, mas n�o o endere�o apontado por s e r.

    return 0;
}
