#ifndef BUTTON_H
#define BUTTON_H
#include <iostream>

class Button
{
    public:
        virtual void paint() = 0;
};

class OSXButton : public Button
{
    public:
        void paint()
        {
            std::cout << "OSX Button" << std::endl;
        }
};

class WindowsButton : public Button
{
    public:
        void paint()
        {
            std::cout << "Windows Button" << std::endl;
        }
};

#endif // BUTTON_H
