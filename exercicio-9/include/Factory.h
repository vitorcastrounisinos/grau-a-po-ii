#ifndef FACTORY_H
#define FACTORY_H
#include <cstring>
#include "Button.h"

class GUIFactory {

    public:
        virtual Button *createButton(char *) = 0;

};

class Factory : public GUIFactory
{
    public:
        Button *createButton(char *type) {
            if(strcmp(type, "Windows") == 0) {
                return new WindowsButton;
            }
            else if(strcmp(type, "OSX") == 0) {
                return new OSXButton;
            }
        }
};

#endif // FACTORY_H
