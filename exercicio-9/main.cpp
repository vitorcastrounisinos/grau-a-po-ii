#include <iostream>
#include "Factory.h"
#include "Button.h"

using namespace std;

int main()
{
    GUIFactory* guiFactory;
    Button* btn;
    guiFactory = new Factory;

    btn = guiFactory->createButton("OSX");
    btn->paint();
    btn = guiFactory->createButton("Windows");
    btn->paint();

    return 0;
}
