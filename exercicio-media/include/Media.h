#ifndef MEDIA_H
#define MEDIA_H

class media
{
    public:
        media();
        virtual ~media();

        float get_ga();
        float get_gb();

        void setGa(float);
        void setGb(float);

        float calcular_media();
        float calcular_gc();

    private:
        float ga;
        float gb;
};

#endif // MEDIA_H
