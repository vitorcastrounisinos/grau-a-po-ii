#include <iostream>
#include <iomanip>
#include "Media.h"

using namespace std;

int main()
{
   float notaGa, notaGb;

    media calculoNota;

    cout << "Digite a nota do Grau A: " << endl;
    cin >> notaGa;
    cout << "Digite a nota do Grau B: " << endl;
    cin >> notaGb;

    calculoNota.setGa(notaGa);
    calculoNota.setGb(notaGb);


    cout << setprecision(3);
    cout<<"A media eh: "<<calculoNota.calcular_media()<<endl;


    if(calculoNota.calcular_media()<6)
    {
        cout<<"Voce precisa de recuperacao "<<endl;
        cout<<calculoNota.calcular_gc()<<endl;

    }else{
        cout<<"Voce esta aprovado"<<endl;
    }
}
