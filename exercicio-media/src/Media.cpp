#include <iostream>
#include "Media.h"
using namespace std;
media::media()
{
    //ctor
}

media::~media()
{
    //dtor
}

float media::get_ga()
{
    return ga;
}
float media::get_gb()
{
    return gb;
}

void media::setGa(float grau)
{
    if(grau >= 0 && grau <= 10)
        ga = grau;
    else
        ga = 0;
}

void media::setGb(float grau)
{
    if(grau >= 0 && grau <= 10)
        gb = grau;
    else
        gb = 0;
}

float media::calcular_media()
{
    return (ga + (2*gb))/3;
}

float media::calcular_gc()
{
    float nota_substitui_ga = 18 - 2*gb;
    float nota_substitui_gb = (18 - ga)/2;

    if(nota_substitui_ga > nota_substitui_gb)
	{
		cout<<"voce precisa substituir o GB por uma nota maior ou igual a: "<<endl;
        return nota_substitui_gb;
	}
    else{
		cout<<"voce precisa substituir o GA por uma nota maior ou igual a: "<<endl;
        return nota_substitui_ga;
	}
}
